library(optparse)

option_list = list(
        make_option(c("-s", "--sample_sites"), type="character", default=NULL,
                help="Sample specific sites", metavar="character"),

        make_option(c("-p", "--pCADD_scores"), type="character", default=NULL,
                help="pCADD scores", metavar="character"),

        make_option(c("-o", "--output"), type="character", default=NULL,
                help="Output file containing sample specific scores", metavar="character")
);

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

sample_sites <- read.table(opt$sample_sites, header=F)
colnames(sample_sites) <- c("chr", "location", "ref", "alt", "homozygote")

pCADD_scores <- read.table(opt$pCADD_scores, header=F, sep="\t")
colnames(pCADD_scores) <- c("chr", "location", "ref", "alt", "homozygote")

sample_scores <- merge(
	sample_sites, pCADD_scores, 
	by = c("chr", "location", "ref", "alt"))

write.table(sample_scores, opt$output, row.names = F, sep="\t", quote=F)
