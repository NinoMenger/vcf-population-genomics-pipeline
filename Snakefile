"""
Created by: Nino Menger
Date: 17/02/2023
Python version:
Snakemake version:
vcftools version:

Description:
"""

# Reffrence to the config file. The user can change some settings within this file.
configfile: "snakefileConfig.yaml"

def get_samples():
	list = []
	for i in config["GROUPS"].values():
		for j in i.split(" "): 
			list.append(j)
	print(list)
	return list

rule all:
	input: 
		expand("Subset/Multi_VCFs_Sscrofa_Chr{CHR}.vcf.gz",CHR=["1", "2", "X"])
		#expand(config["SMCppPLOT"], group=config["GROUPS"].keys()),
		#expand("plots/smcpp/combi_{combi}.png", combi=config["COMBINATIONS"].keys())
		#"output/inbreeding/roh_plink", "output/inbreeding/roh_bcf",
		#"output/pCADD/pCADD_WB26M12"
		#expand("output/pCADD/pCADD_{sample}", sample=get_samples())
		#expand("output/pCADD/pCADD_base_chr{chr}", chr= list(range(1, 19)))
		#"output/pCADD/pCADD_WB26M12_all"
		#expand("output/pCADD/pCADD_{sample}_all", sample=get_samples())


rule subset:
	input:
		config["VCFs"]
	output:
		[config["SUBSET"] + "." + i for i in ["vcf.gz", "recoded.vcf.gzary", "recoded.vcf.gzary.tbi"]],
		temp([config["SUBSET"] + ".recoded." + i for i in ["log", "nosex"]])
	params:
		base = config["SUBSET"],
		samples = ",".join([",".join(i.split(" ")) for i in config["GROUPS"].values()])
	shell:
		"""
		# Load modules
		module load bcftools
		module load plink
		
		echo {params.samples}

		# Create subset
		bcftools view {input} -s {params.samples} -O z -o {params.base}.vcf.gz

		# Recode subset
		plink --vcf {params.base}.vcf.gz --double-id --keep-allele-order \
			--recode vcf-iid bgz -out {params.base}.recoded

		# Create index of recoded subset
		tabix -p vcf {params.base}.recoded.vcf.gzary

		# Unload modules
		module unload bcftools
		module unload plink
		"""


def get_pop(wildcards):
	return wildcards[0] + ":" + config["GROUPS"].get(wildcards[0]).replace(" ", ",")


def get_alternate_chr_alias(wildcards):
	try:
		return config["HCVCF"][config["HC"].index(wildcards[1])]
	except:
		return wildcards[1]


rule vcf2smc:
	input:
		(config["SUBSET"] + ".recoded.vcf.gzary")
	output:
		config["SMCppFILE"]
	params:
		chr = "{CHR}",
		chrvcf = get_alternate_chr_alias,
		popu = get_pop
	shell:
		"""
		smc++ vcf2smc {input} {output} (params.chrvcf) {params.popu} 
		"""


rule smcEstimate:
	input:
		expand(config["SMCppFILE"], group="{group}", CHR=[ i + 1 for i in range(1, config["AC"]+1)] + config["HC"])
	output:
		(config["SMCppESTIMATE"] + "{group}.final.json")
	params:
		mutationrate = config["SMCppMUTATIONRATE"],
		knots = config["SMCppKNOTS"],
		ftol = config["SMCppFTOL"],
		outputdir = config["SMCppESTIMATE"],
		base = "{group}"
	shell:
		"""
		smc++ estimate {params.mutationrate} {input} -o \
			{params.outputdir} --base {params.base} \
			--knots {params.knots} --ftol {params.ftol}
		"""


def get_title(wildcards):
        return config["TITLES"].get(wildcards[0])

rule smcPlot:
	input: (config["SMCppESTIMATE"] + "{group}.final.json")
	output: 
		final = config["SMCppPLOT"],
		iters = config["SMCppITERS"]
	params:
		dir = config["SMCppESTIMATE"],
		group = "{group}",
		gentime = config["SMCppGENERATIONTIME"],
		title = get_title
	shell:
		"""
		smc++ plot -g {params.gentime} -c {output.final} {input}
		smc++ plot -g {params.gentime} -c {output.iters} $(ls {params.dir}/.{params.group}*)
		
		# Load modules
		module unload gcc
        module load R/4.2.0

		Rscript Scripts/smcpp.R --final {output.final} --iterations {output.iters} --title "{params.title}"

		# Unload modules
                module unload R
		"""

def get_combi_files(wildcards):
	return [config["SMCppESTIMATE"] + i + ".final.json" for i in config["COMBINATIONS"].get(wildcards[0])[:-1]]

def get_combi_title(wildcards):
	return config["COMBINATIONS"].get(wildcards[0])[-1]

rule smcPlotToghether:
	input:	get_combi_files
	output: config["SMCppCOMBI"]
	params:
		gentime = 5,
		title = get_combi_title
	shell:
		"""
		base={output}
		smc++ plot -g {params.gentime} -c ${{base::-4}} {input}
		
		# Load modules
                module unload gcc
                module load R/4.2.0

		Rscript Scripts/smcppCombi.R --final {output} --title "{params.title}"
		
		# Unload modules
		module unload R
		"""


rule het:
	input:
		vcf = expand(config["SUBSET"] + ".recoded.vcf.gzary", CHR = list(range(1, config["AC"]+1)) + config["HC"]),
		rohplink = config["ROHPLINKOUT"],
		rohbcf = config["ROHBCFOUT"]
	output:
		het = config["HET"]#,
		#plot = config["HETPLOT"]
	params:
		sample = "{SAMPLE}",
		bin_size = config["BINSIZE"],
		chromosomes = (list(range(1, config["AC"])) + config["HC"])
	shell:
		"""
		module load vcftools
		module load R

		touch {output.het}

		for input in {input.vcf}
		do

		vcftools --gzvcf $input --window-pi {params.bin_size} --indv {params.sample} --stdout >> {output.het}

		done

		#Rscript Scripts/ROHcircle.R --plink {input.rohplink} --bcf {input.rohbcf} --het {output.het} \
		#	--chrLengths  --sampleName {params.sample} --output {output.plot}
		"""



rule roh_plink:
	input: (config["SUBSET"] + ".vcf.gz")
	output: 
		(config["ROHPLINKOUT"] + "{CHR}.hom"),
		temp([config["ROHPLINKOUT"] + "{CHR}" + i for i in [".nosex", ".log", ".hom.summary", ".hom.indiv"]])
	params: 
		OutputBase = (config["ROHPLINKOUT"] + "{CHR}"),
		homozygkb = config["HOMOZYGKB"],
		windowsnp = config["WINDOWSNP"],
		windowmissing = config["WINDOWMISSING"],
		windowhet = config["WINDOWHET"]
	shell:
		"""
		module load plink
		plink --vcf {input} --homozyg-kb {params.homozygkb} --homozyg-window-snp {params.windowsnp} --double-id \
                        --homozyg-window-missing {params.windowmissing} --homozyg-window-het {params.windowhet} \
                        --out {params.OutputBase}
		"""

rule roh_plink_combine:
	input: expand(config["ROHPLINKOUT"] + "{CHR}.hom", CHR=list(range(1, config["AC"] + 1)) + config["HC"])
	output: config["ROHPLINKOUT"]
	shell:
		"""
		awk '{{if(($1=="FID" && NR == 1) || $1!="FID"){{print $0}}}}' {input} > {output}
		"""

rule roh_bcf:
	input: (config["SUBSET"] + ".vcf.gz")
	output: (config["ROHBCFOUT"] + "{CHR}")
	params: config["BCFGE"]
	shell:
		"""
		module load bcftools
		bcftools roh -G {params} -O r {input} > {output}
		"""

rule roh_bcf_combine:
	input: expand(config["ROHBCFOUT"] + "{CHR}", CHR=list(range(1, config["AC"] + 1)) + config["HC"])
	output: config["ROHBCFOUT"] 
	shell:
		"""
		cat {input} > {output}
		"""




rule fROH:
	input: config["ROHBCFOUT"]
	output: config["fROH"]
	params:
		chrLength = sum(config["CHROMOSOME_LENGTHS"]),
		families = str(config["GROUPS"]).strip("{}").replace(": ",":").replace(",", ";").replace(" ", ",").replace(",'", "").replace("'", "")
	shell:
		"""
		module load R
		echo "{params.families}"
		Rscript Scripts/rROH.R --ROHfile {input} --plot {output} --chrLength {params.chrLength} \
			--families "{params.families}"
		"""


rule pCADD:
	input: (config["SUBSET"] + ".recoded.vcf.gzary")
	output: 
		vcf = temp(".temp{CHR}.vcf"),
		pCADD = config["PCADDBASE"]
	params: 
		pCADD_SNP_script = config["PCADDSNPSCRIPT"],
		pCADD_phred = config["PCADDPHRED"],
		pCADD_columns = config["pCADDCOLUMNS"],
		chromosome = "{CHR}"
	shell:
		"""
		module load bcftools
                bcftools view {input} -O v -o {output.vcf}
		python {params.pCADD_SNP_script} \
                        --infile {output.vcf} \
                        --column "{params.pCADD_columns}" \
                        --pCADDfile {params.pCADD_phred}{params.chromosome}_pCADD-PHRED-scores.tsv.gz > {output.pCADD}
		
		# Rewrite warning messages as NA data
		cat {output.pCADD} | awk '{{if($0~/Warning/){{split($0,s1, ":");\
				     split($0,s2,","); chr_length=split(s1[1], chr, " "); \
				     ref_length=split(s2[1], ref, " "); split(s2[2], alt, " "); \
				     print chr[chr_length] "\t" s1[2] "\t" ref[ref_length] "\t" alt[1] "\tNA"}} \
				     else{{print$0}}}}' > {output.pCADD}
		"""	


rule pCADD_extract_samples:
	input:
		pCADD = config["PCADDBASE"],
		vcf = (config["SUBSET"] + ".recoded.vcf.gzary")
	output:
		pCADD = temp(config["PCADDSAMPLE"] + "{CHR}"),
		sites = temp(".temp_{SAMPLE}_{CHR}_sites") 
	params:
		"{SAMPLE}"
	shell:
		"""
		# Load modules
		module unload bcftools
		module load bcftools
                module unload gcc
                module load R/4.2.0
		
		# Extract sample specific sites that contain atleast one alterative allele
		bcftools view {input.vcf} -s {params} -O v | \
			awk -F '\t' '{{if(substr($0, 0, 2) != "##"){{print $1"\t"$2"\t"$4"\t"$5"\t"$10}}}}' | \
			awk '{{if($5~/1/){{print $0}}}}' > {output.sites}
		
		# R codes that merges the sample specific sites with the pCADD scores
		Rscript scripts/pCADD.R --sample_sites "{output.sites}" \
					--pCADD_scores "{input.pCADD}" \
					--output "{output.pCADD}"
		"""



