# VCF Quality and Population Structure Pipeline

## Author information
Author: Nino Menger

Under the guidance of: Mirte Bosse

Date finisched: To Be Finished


## Warning!
The Annuna cluster was upgraded to a Ubuntu-based login node. Sadly, at the time of writing (15/02/2023), envoirement modules do not seem to work yet while running jobs though the SLURM system. 

### Solution
The problem has been reported at the Annuna maintenance team on: 15/02/2023

As a temporary work around, the pipeline can be ran by logging into the old login node: login1.anunna.wur.nl


## Run the pipeline
The pipeline is build to run on the Anunna cluster using the SLURM system to queue jobs. To run it the following steps must be followed:

### 1- Download the entire project and place it on the Anunna cluster.
Can be achieved by the following command:

git clone https://git.wur.nl/NinoMenger/vcf-quality-and-population-structure-pipeline

### 2- Make sure your multi-sample VCF data is ready for use.

The VCF data must contain multiple samples and must be seperated by chromosome over multiple VCF files.

Futhermore, a tab seperated annotation file must be present. This anootation file can contain a maximum of five columns, whereof the first one contains the sample identifiers. The other four columns can be used to classefy the samples as the user wishes. For the orginal project the following four calssification columns were used: species, domestication status, continental origin and specific origin. For the 'Multisample_VCFs_Sscrofa11.1' data set, which is used to create the pipeline, a script capeable of converting the orinial   annotation file to a more orginised one is included.

### 3- Setup the 'SnakefileConfig.yaml' file

This file is used to give the user the possibility to setup the pipeline according to their wishes. For the 'Multisample_VCFs_Sscrofa11.1' data set, which is used to create the pipeline, all settings are already optimised. For other datasets, the variables listed down below have to be changed. The downloaded config file can be used as an example how the settings should look like.
